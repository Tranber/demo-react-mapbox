# Vianova coding challenge

## Starting on localhost

Install dependencies:

```sh
npm install
```

For quick start (without build) with webpack-dev-server, use command below.

Start with hot module reloading:

```sh
npm start
```

## Building

Production build:

```sh
npm run build:prod
```

Developpement build:

```sh
npm run build:dev
```

Build files are in dist directory.
They can be served with any web server (like http-server, apache, nginx)

Le fichier .env contient le token Mapbox (MAPBOX_ACCESS_TOKEN)

Au survol, les markers affichent un popup d'infos.
Le calques des districts et les markers des stations peuvent être affichés/masqués avec les boutons de gauche.
